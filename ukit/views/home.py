from django.views.generic import View
from django.shortcuts import render_to_response
from django.template import RequestContext


class HomeView(View):

    def get(self, request):

        # TODO Project 0
        return render_to_response('home.html',
            {'message': 'Hi!'}, context_instance=RequestContext(request))
